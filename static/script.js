/*
 * Configuration page script.
 * This module interacts with the /config endpoint to display, modify, and
 * reset config data on the device.
 */
var config = (function() {
    var FIRMWARE_PATH = '/firmware';
    var CONFIG_PATH = '/config';
    var REBOOT_DELAY_MILLIS = 10000;

    // make an async HTTP request
    function sendRequest(method, path, postData, onDone) {
        var request = new XMLHttpRequest();

        request.onreadystatechange = function() {
            if (request.readyState == request.DONE) {
                if (request.status === 200) {
                    // call handler on success
                    onDone(request);
                } else if (request.status !== 0) {
                    // show error message from response
                    window.alert("Error: " + request.response);
                } else {
                    window.alert("Error communicating with device.");
                }
            }
        };

        request.open(method, path, true);

        if (postData) {
            request.setRequestHeader('Content-Type', postData.contentType);
            request.send(postData.body);
        } else {
            request.send();
        }
    }

    // convert a query string into an object
    function parseQueryString(string) {
        var object = {};
        var pairs = string.split('&');
        for (var i = 0, length = pairs.length; i < length; ++i) {
            var pair = pairs[i].split('=');
            if (pair.length != 2) {
                throw new Error('Malformatted query string data');
            }
            var key = decodeURIComponent(pair[0]);
            var value = decodeURIComponent(pair[1]);
            object[key] = value;
        }
        return object;
    }

    // convert object into a query string
    function formatQueryString(object) {
        var pairs = [];
        var properties = Object.keys(object);
        for (var i = 0, length = properties.length; i < length; ++i) {
            var property = properties[i];
            var key = encodeURIComponent(property);
            var value = encodeURIComponent(object[property]);
            pairs.push(key + '=' + value);
        }
        return pairs.join('&');
    }

    // extract form field values into an object
    function serializeForm(form) {
        var object = {};
        var fields = form.querySelectorAll(
            'input[type="text"], input[type="password"]');
        for (var i = 0, length = fields.length; i < length; ++i) {
            var field = fields[i];
            if (field.name) {
                object[field.name] = field.value;
            }
        }
        return object;
    }

    // populate form field values from object properties
    function populateForm(form, object) {
        var properties = Object.keys(object);
        for (var i = 0, length = properties.length; i < length; ++i) {
            var property = properties[i];
            var fields = form.querySelectorAll(
                'input[name="' + property + '"]');
            if (fields.length == 1) {
                var value = object[property];
                fields[0].value = value;
            }
        }
    }    

    // replace form with message
    function disableForm(form, messageText, message) {
        // prevent further use of the UI on a disconnected device
        form.style.display = 'none'; // hide form
        messageText.innerText = message;
        messageText.style.display = 'block'; // show message
    }

    // populate the firmware info area
    function loadFirmware(firmwareText) {
        sendRequest('GET', FIRMWARE_PATH, null,
            function(request) {
                try {
                    firmware = parseQueryString(request.response);
                    firmwareText.innerText =
                        firmware.title + ' ' + firmware.version;
                } catch(exception) {
                    window.alert(exception.message); // show error
                }
            });
    }

    // populate the form from the device config
    function loadConfig(form) {
        sendRequest('GET', CONFIG_PATH, null,
            function(request) {
                try {
                    config = parseQueryString(request.response);
                    populateForm(form, config);
                } catch(exception) {
                    window.alert(exception.message); // show error
                }
            });
    }

    // post the form config to the device and wait for a reboot
    function saveConfig(form, messageText) {
        // extract form fields and format as query string
        var config = serializeForm(form);
        var formattedConfig = formatQueryString(config);
        var postData = {
            contentType: "application/x-www-form-urlencoded",
            body: formattedConfig
        };

        // POST query string to config endpoint
        sendRequest('POST', CONFIG_PATH, postData,
            function(request) {
                // replace form with message
                disableForm(form, messageText,
                    'Saving configuration and rebooting, please wait.');

                // wait for reboot and reload
                window.setTimeout(function() {
                    location.reload()
                }, REBOOT_DELAY_MILLIS);
            });
    }

    // reset the config to the default values
    function resetConfig(form, messageText) {
        // confirm with dialog
        if (!window.confirm("Erase all settings and boot into client mode?")) {
            return;
        }

        sendRequest('DELETE', CONFIG_PATH, null,
            function(request) {
                disableForm(form, messageText,
                    'Rebooting into client mode. Please close this page.');
            });
    }

    return {
        init: function(selector) {
            var config = document.querySelector(selector);
            var firmwareText = config.querySelector(".config-firmware");
            var form = config.querySelector(".config-form");
            var resetButton = config.querySelector(".config-reset");
            var saveButton = config.querySelector(".config-save");
            var messageText = config.querySelector(".config-message");
            var lcdLine1 = config.querySelector(".config-lcd-line");

            // register reset button
            resetButton.addEventListener('click', function(event) {
                resetConfig(form, messageText);               
            });

            // register apply button 
            saveButton.addEventListener('click', function(event) {
                saveConfig(form, messageText);
            });

            // populate page
            loadFirmware(firmwareText);
            loadConfig(form);
        }
    }
})();
