/**
 * Configuration struct and parsing/storage objects.
 */
#ifndef CONFIG_H
#define CONFIG_H

// only used for LCD buffer size constant
#include "hardware.h"

#include <cstdint>
#include <cstddef>


/**
 * Macro for determining the max length of a config field string in characters.
 */
#define CONFIG_STRLEN(member) (sizeof(((config_values*)0)->member) - 1)

/**
 * Default lcd_line_values values.
 */
#define LCD_LINE_DEFAULT_VALUES {       \
    "",           /* topic */           \
    "",           /* selector */        \
    "",           /* prefix */          \
    "",           /* suffix */          \
    90,           /* timeout */         \
}

/**
 * Default config_values values.
 */
#define CONFIG_DEFAULT_VALUES {         \
    "",           /* wifiHostname */    \
    "",           /* wifiSsid */        \
    "",           /* wifiPassword */    \
                                        \
    "",           /* mqttHost */        \
    1883,         /* mqttPort */        \
    "",           /* mqttUsername */    \
    "",           /* mqttClientId */    \
    "",           /* mqttTopic */       \
                                        \
    30,           /* lcdBacklight */    \
    /* lcdLineValues */                 \
    {                                   \
        LCD_LINE_DEFAULT_VALUES,        \
        LCD_LINE_DEFAULT_VALUES,        \
        LCD_LINE_DEFAULT_VALUES,        \
        LCD_LINE_DEFAULT_VALUES,        \
    },                                  \
}

/**
 * Structure containing configuration for a single LCD line.
 * The +1 on array sizes is just to make it easier to visually determine
 * the field content width since a null character will be added.
 */
typedef struct lcd_line_values {
    char topic[64 + 1];
    char selector[64 + 1];
    char prefix[LCD_LINE_LENGTH + 1];
    char suffix[LCD_LINE_LENGTH + 1];
    uint16_t timeoutSecs;
} lcd_line_values;

/**
 * Structure containing all config values.
 * The +1 on array sizes is just to make it easier to visually determine
 * the field content width since a null character will be added.
 */
typedef struct config_values {
    // WiFi (sizes based on ESP8266WiFi)
    char wifiHostname[24 + 1];
    char wifiSsid[32 + 1];
    char wifiPassword[63 + 1];

    // MQTT
    char mqttHost[64 + 1];
    uint16_t mqttPort;
    char mqttUsername[64 + 1];
    char mqttClientId[23 + 1];
    char mqttTopic[64 + 1];

    // LCD
    uint8_t lcdBacklightSecs;
    lcd_line_values lcdLineValues[LCD_ROWS];

} config_values;


// size of buffer used to hold formatted values and error messages
const size_t CONFIG_BUFFER_SIZE = 64 + 1;

/**
 * Parses and formats config fields from strings.
 * Pointers to value and error strings returned from methods are only valid until
 * another method is called.
 */
class StringConfig {
    public:
        /**
         * List of user-configurable field names.
         * This should be set to the names of the fields in the config_values
         * struct, but converted to kebab-case: wifiSsid -> wifi-ssid .
         */
        static const char* const USER_FIELDS[];
        static const size_t USER_FIELD_COUNT;

        /**
         * Create a formatter/parser for the specified config_values struct.
         */
        StringConfig(config_values* values);

        /**
         * Format the specified field into a string.
         */
        const char*  format(const char* fieldName);

        /**
         * Parse the specified field from a String value.
         * An error message will be returned on failure.
         */
        const char* parse(const char* field, const char* value);

        /**
         * Check if all required fields have been set.
         * An error message will be returned on failure.
         */
        const char* isValid();
    private:
        config_values* values;
        char buffer[CONFIG_BUFFER_SIZE];
        const char* parseString(const char* title, const char* value, char* configDest, size_t maxLength);
        const char* parseNumber(const char* title, const char* value, long* result, long minValue, long maxValue);
        const char* bprintf(const char* format, ...);
        bool isPrintableString(const char* value);
};

#endif
