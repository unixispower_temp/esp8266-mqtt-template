/**
 * Main file for ESP8266 MQTT firmware.
 */
#include "server.h"
#include "client.h"
#include "reset.h"
#include "eeprom.h"
#include "hardware.h"

#include <ESP8266WiFi.h>

#include <cstdint>


// WiFi access point settings
const char AP_SSID_PREFIX[] = "LCD:";  // suffixed by WiFi MAC
const char AP_PASSWORD[] = "password";  // must be 8+ chars

// network settings
const IPAddress AP_IP(192, 168, 1, 1);  // address of web interface
const IPAddress AP_GATEWAY(192, 168, 1, 1);
const IPAddress AP_SUBNET(255, 255, 255, 0);


ConfigServer configServer;
MqttClient mqttClient;

uint8_t mode; // operation mode


void setup() {
    hardware_setup();

    // set global WiFi settings
    WiFi.persistent(false);  // already saved in flash, see config.h
    WiFi.mode(WIFI_OFF); // disable until a mode is entered

    // display firmware info
    Lcd.print(FIRMWARE_SHORT_TITLE);
    Lcd.setCursor(0, 1);
    Lcd.print("FW " FIRMWARE_VERSION);

    // read mode from stored config
    EepromStore.begin();  // set defaults on first boot and handle upgrades
    mode = EepromStore.getMode();

    // handle resets
    uint8_t totalResets = ResetDetector.count();
    if (totalResets == 3) {
        mode = MODE_CONFIG;
        BuiltinLed.blink(3);
        delay(500); // separate reset blinks from mode blinks
    } else if (totalResets == 5) {
        EepromStore.writeDefaults();
        mode = EepromStore.getMode();
        BuiltinLed.blink(5);
        delay(500); // separate reset blinks from mode blinks
    }

    // start appropriate mode
    const config_values* configValues = EepromStore.getConfigPtr();
    Lcd.clear();
    if (mode == MODE_CONFIG) {
        Lcd.print("Config mode");

        // generate SSID of prefix + MAC
        uint8_t mac[6];
        char ssid[sizeof(AP_SSID_PREFIX) + sizeof(mac) * 2];
        WiFi.macAddress(mac);
        sprintf(ssid, "%s%02X%02X%02X%02X%02X%02X",
            AP_SSID_PREFIX, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

        // bring WiFi up in soft AP mode
        WiFi.mode(WIFI_AP);
        WiFi.softAPConfig(AP_IP, AP_GATEWAY, AP_SUBNET);
        WiFi.softAP(ssid, AP_PASSWORD);

    } else if (mode == MODE_CLIENT) {
        Lcd.print("Client mode");

        // bring WiFi up in STA mode
        WiFi.mode(WIFI_STA);
        WiFi.hostname(configValues->wifiHostname);
        WiFi.begin(configValues->wifiSsid, configValues->wifiPassword);

        mqttClient.setup();
    }

    configServer.setup();
}

void loop() {
    // MQTT client only runs in client mode
    if (mode == MODE_CLIENT) {
        mqttClient.loop();
    }

    // config server always runs
    configServer.loop();
}
