#include "eeprom.h"

#include <cstring>
#include <algorithm>

#include <EEPROM.h>
#include <md5.h>

// max for this is SPI_FLASH_SEC_SIZE
#define EEPROM_BUFFER_SIZE 2048


EepromStoreClass::EepromStoreClass() {}

void EepromStoreClass::begin() {
    // load as much of the EEPROM as possible -- different firmware versions may use more
    // or less EEPROM than this one. calcHash() and migrate() need to access to memory
    // that may be out of scope of the current firmware's EEPROM usage
    EEPROM.begin(EEPROM_BUFFER_SIZE);
    eeprom_values* storedValues = this->getBufferPtr();

    // invalid hash check -- assume EEPROM is unitialized or incompatible
    if (!this->isValid()) {
        this->writeDefaults();
        return;
    }

    // different firmware build detected -- assume incompatible
    if (strcmp(storedValues->name, FIRMWARE_NAME) != 0) {
        this->writeDefaults();
        return;
    }

    // same firmware, different version -- assume compatible enough to migrate
    if (strcmp(storedValues->version, FIRMWARE_VERSION) != 0) {
        this->migrate();
    }
}

void EepromStoreClass::commit() {
    this->calcHash(this->getBufferPtr()->hash);
    EEPROM.commit();
}

uint8_t EepromStoreClass::getMode() {
    return this->getBufferPtr()->mode;
}

void EepromStoreClass::setMode(uint8_t mode) {
    this->getBufferPtr()->mode = mode;
}

const config_values* EepromStoreClass::getConfigPtr() {
    return &this->getBufferPtr()->configValues;
}

void EepromStoreClass::getConfig(config_values* values) {
    memcpy(values, &this->getBufferPtr()->configValues, sizeof(config_values));
}

void EepromStoreClass::setConfig(const config_values* values) {
    memcpy(&this->getBufferPtr()->configValues, values, sizeof(config_values));
}

void EepromStoreClass::writeDefaults() {
    eeprom_values defaultValues = EEPROM_DEFAULT_VALUES;
    memcpy(this->getBufferPtr(), &defaultValues, sizeof(eeprom_values));
    this->commit();
}

// return a pointer to the EEPROM buffer as eeprom_values
// necesary because EEPROM memory is only flagged "dirty" when EEPROM.getDataPtr() is called
eeprom_values* EepromStoreClass::getBufferPtr() {
    return (eeprom_values*)EEPROM.getDataPtr();
}

// compare stored checksum with calculated checksum to determine EEPROM data integrity
bool EepromStoreClass::isValid() {
    // calculate the hash of the current EEPROM buffer
    uint8_t calculatedHash[EEPROM_HASH_SIZE];
    this->calcHash(calculatedHash);

    // compare to hash stored in buffer
    int compare = memcmp(&this->getBufferPtr()->hash, calculatedHash, EEPROM_HASH_SIZE);
    return compare == 0;
}

// copy a previous firmware's EEPROM values into a fresh eeprom_values struct and commit it
void EepromStoreClass::migrate() {
    // start with the default values for this firmware
    eeprom_values* oldValues = this->getBufferPtr();
    eeprom_values newValues = EEPROM_DEFAULT_VALUES;

    // copy mode
    newValues.mode = oldValues->mode;

    // copy as much of the existing struct into the new struct as possible
    // WARNING: this is pretty naiive -- it will only work if the new struct is EXACTLY
    // like the old struct but with fields added to or removed from the end of the struct
    // ADDING OR REMOVING FIELDS IN THE MIDDLE OF THE STRUCT WILL CAUSE MISALIGNMENT
    size_t smallerConfigSize = std::min((size_t)oldValues->configSize, sizeof(config_values));
    memcpy(&newValues.configValues, &oldValues->configValues, smallerConfigSize);

    // replace the old buffered values and commit to flash
    memcpy(oldValues, &newValues, sizeof(eeprom_values));
    this->commit();
}

// calculate hash of values in EEPROM buffer
void EepromStoreClass::calcHash(uint8_t* hash) {
    eeprom_values* bufferValues = this->getBufferPtr();

    // this is truncated to the maximum size of config_values that will fit in EEPROM
    size_t storedConfigSize = bufferValues->configSize;
    size_t maxConfigSize = EEPROM_BUFFER_SIZE - offsetof(eeprom_values, configValues);
    uint16_t configSize = std::min(storedConfigSize, maxConfigSize);

    // calculate the position and size of memory that needs to be hashed
    uint8_t* beginLocation = (uint8_t*)&bufferValues->name;  // first value after hash
    uint8_t* configLocation = (uint8_t*)&bufferValues->configValues;  // start of variable-size region
    size_t hashRegionSize = (configLocation - beginLocation) + configSize;

    md5_context_t hashContext;
    MD5Init(&hashContext);
    MD5Update(&hashContext, beginLocation, hashRegionSize);
    MD5Final(hash, &hashContext);
}


// global definitions
EepromStoreClass EepromStore;
