/**
 * Hardware globals.
 */
#ifndef HARDWARE_H
#define HARDWARE_H

#include "led.h"

#include "src/liquidcrystal-pcf8574/LiquidCrystal_PCF8574.h"

#include <cstdint>


/**
 * Hardware locations.
 */
const uint8_t BUILTIN_LED_PIN = 16;  // NodeMCU LED
const uint8_t BACKLIGHT_BUTTON_PIN = 0;  // NodeMCU FLASH button

/**
 * LCD constants.
 * LCD_COLUMNS and LCD_ROWS determine config sizes so changing here will require
 * changes in config.cpp (and will break backwards compatability). These settings
 * will work with as-is with a 16x2 LCD; just ignore lines 3 and 4 in the web interface.
 */
const uint8_t LCD_LINE_LENGTH = 20;  // always 20 for HD44780
const uint8_t LCD_ADDRESS = 0x3F;
const uint8_t LCD_COLUMNS = 20;
const uint8_t LCD_ROWS = 4;


/**
 * Initialize hardware.
 */
void hardware_setup();


extern Led BuiltinLed;
extern LiquidCrystal_PCF8574 Lcd;

#endif
