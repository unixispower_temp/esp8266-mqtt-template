#include "led.h"

#include <Arduino.h>


const uint16_t BLINK_PERIOD_MILLIS = 200;
const uint16_t FAST_BLINK_PERIOD_MILLIS = 80;
const uint16_t BLIP_PERIOD_MILLIS = 10;

Led::Led(uint8_t pin)
    : pin(pin) {}

void Led::begin() {
    this->off(); // turn off before setting direction to avoid short flash
    pinMode(this->pin, OUTPUT);
}

void Led::on() {
  digitalWrite(this->pin, LOW);
}

void Led::off() {
  digitalWrite(this->pin, HIGH);
}

void Led::blink(uint8_t count) {
    this->blinkDelay(count, BLINK_PERIOD_MILLIS);
}

void Led::blinkFast(uint8_t count) {
    this->blinkDelay(count, FAST_BLINK_PERIOD_MILLIS);
}

void Led::blinkFastForSecs(uint8_t seconds) {
    uint32_t startMillis = millis();
    uint32_t elapsedMillis = 0;
    while (elapsedMillis / 1000 < seconds) {
        this->blinkDelay(1, FAST_BLINK_PERIOD_MILLIS);
        elapsedMillis = millis() - startMillis;
    }
}

void Led::blip() {
    this->blinkDelay(1, BLIP_PERIOD_MILLIS);
}


// blink with the specified delay
void Led::blinkDelay(uint8_t count, uint16_t delayMillis) {
    while (count--) {
        digitalWrite(this->pin, LOW);  // on
        delay(delayMillis);
        digitalWrite(this->pin, HIGH);  // off
        delay(delayMillis);
    }
}
